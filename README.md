# Pexeso

Hra pexeso je jednoduchá elektronická verze známé stejnojmenné karetní hry.
Počáteční požadavek byl vytvořit rychle jednoduchý základ hry, který se možná někdy rozšíří ;-)

## Instalace a spuštění verze pro vývoj

```
git clone git@bitbucket.org:SwedCZ/pexeso.git Pexeso
cd Pexeso
sudo npm install
bower install
gulp
```