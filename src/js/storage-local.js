// Class StorageLocal
var StorageLocal = function($, Storage) {
    /**
     * Class StorageLocal
     * @returns {StorageLocal}
     */
    function StorageLocal() {}

    /**
     * Get variable from storage
     * @param {String} key
     * @returns {mix}
     */
    StorageLocal.prototype.get = function(key) {
        return localStorage.getItem(key);
    };

    /**
     * Get parsed JSON from storage
     * @param {String} key
     * @returns {Object|Array}
     */
    StorageLocal.prototype.getStruct = function(key) {
        return JSON.parse(localStorage.getItem(key));
    };

    /**
     * Stringigy to JSON and store
     * @param {String} key
     * @param {Object|Array} value
     */
    StorageLocal.prototype.setStruct = function(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    };

    /**
     * Store variable in storage
     * @param {String} key
     * @param {mix} value
     */
    StorageLocal.prototype.set = function(key, value) {
        localStorage.setItem(key, value);
    };

    /**
     * Remove variable from storage
     * @param {String} key
     */
    StorageLocal.prototype.remove = function(key) {
        localStorage.removeItem(key);
    };

    /**
     * Remove all variables from storage
     */
    StorageLocal.prototype.removeAll = function() {
        var self = this;
        $.each(localStorage, function(key) {
            self.remove(key);
        });
    };

    return StorageLocal;
}(jQuery, Storage);