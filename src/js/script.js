$(function() {
    // "No JS" warning
    $("html").removeClass("no-js");
    
    // Storage
    var storage = new StorageLocal();
    
    // Memory
    var memory = new Memory({storage: storage, minPairs:4, minCompetitors: 2});
    memory.init();
});