if (confirm("Váš prohlížeč nepodporuje funkce použité na této stránce. Prosím stáhněte si novější prohlížeč.")) {
    //window.location = 'http://browsehappy.com/';
    window.open("http://browsehappy.com/", "Update browser");
    
    if (confirm("Přejete si znovu načíst stránku? (Pokud se vám neotevřelo okno s novými prohížeči ke stažení, možná budete muset povolit otevírání oken.)")) {
        window.location.reload();
    }
}