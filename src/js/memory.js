!function($) {
    // Class Competitors
    var Competitors = function() {
        /**
         * Class Competitors
         */
        function Competitors() {
            this._players = [];
            this.active;
        }

        /**
         * Get player by index
         * @param {Integer} index
         * @returns {Object} Player
         */
        Competitors.prototype.getPlayer = function(index) {
            return this._players[index];
        };

        /**
         * Get winner
         * @returns {Array of Objects} Players
         */
        Competitors.prototype.getWinners = function() {
            var maxScore     = 0,
                winners      = [];

            for (var i = 0; i < this._players.length; i++) {
                var player = this._players[i];

                if (maxScore < player.score) {
                    maxScore = player.score;
                    winners = [player];
                }
                else if (maxScore === player.score) {
                    winners.push(player);
                }
            }

            return winners;
        };

        /**
         * Add new player to game
         * @param {String}  name
         * @param {Integer} score
         * @param {Boolean} active
         * @returns {Object} Player
         */
        Competitors.prototype.addPlayer = function(name, score, active) {
            var player = {
                id     : this._players.length,
                name   : name,
                score  : score || 0,
                active : active || false
            };
            this._players.push(player);
            return player;
        };

        /**
         * Change active player
         * @returns {Object} Player
         */
        Competitors.prototype.nextInLine = function() {
            this.active = this.active && this._players[this.active.id + 1]
                          ? this._players[this.active.id + 1]
                          : this._players[0];
            return this.active;
        };

        /**
         * Find player by name
         * @param {String} name
         * @returns {Object|Boolean} Player|false
         */
        Competitors.prototype.findByName = function(name) {
            for (var i = 0; i < this._players.length; i++) {
                if (name === this._players[i].name) {
                    return this._players[i];
                }
            }
            return false;
        };

        /**
         * Get players count
         * @returns {Integer}
         */
        Competitors.prototype.count = function() {
            return this._players.length;
        };

        /**
         * Export Competitors
         * @returns {Object} Can be stringify to JSON
         */
        Competitors.prototype.export = function() {
            return {
                players: this._players,
                active:  this.active.id
            };
        };

        return Competitors;
    }();

    // Class Game
    var Game = function() {
        /**
         * Class Game
         * @param {Strong} state ['setup', 'canPlay', 'locked', 'goBack', 'done']
         * @returns {Game}
         */
        function Game(state) {
            this._cards  = [];
            this._state;
            this.shownCards = {};

            this.setState(state);
        }

        /**
         * Add card to game
         * @param {String} content
         * @param {String} state ['initial', 'shown', 'locked', 'removed']
         * @returns {Object} Card
         */
        Game.prototype.addCard = function(content, state) {
            var card = {
                key     : this._cards.length,
                content : content,
                state   : state
            };
            this._cards.push(card);
            return card;
        };

        /**
         * Shuffle cards in game
         */
        Game.prototype.shuffleCards = function() {
            var unsortedCards = this._cards;
            this._cards = [];

            for (var count = unsortedCards.length; count > 0; count--) {
                var anotherCard = unsortedCards.splice(Math.floor(Math.random() * count), 1)[0];
                anotherCard.key = this._cards.length;
                this._cards.push(anotherCard);
            }
        };

        /**
         * Get cards from game
         * @returns {Array of Object} Cards
         */
        Game.prototype.getCards = function() {
            return this._cards;
        };

        /**
         * Get card state
         * @par {Object} card Card
         * @returns {String}
         */
        Game.prototype.getCardState = function(card) {
            return card.state;
        };

        /**
         * Get card by key
         * @param {Integer} index
         * @returns {Object} Card
         */
        Game.prototype.getCard = function(index) {
            return this._cards[index];
        };

        /**
         * Get game state
         * @returns {String}
         */
        Game.prototype.getState = function() {
            return this._state;
        };

        /**
         * Set state of game
         * @param {String} state
         * @returns {String} state
         */
        Game.prototype.setState = function(state) {
            this._state = state;
            return this._state;
        };

        /**
         * Change state of card
         * @param {Object} card Card
         * @param {String} state
         */
        Game.prototype.changeCardState = function(card, state) {
            card.state = state;
            switch (state) {
                case 'shown':
                    this.shownCards[card.key] = card;
                    break;
                case 'initial':
                case 'removed':
                    delete this.shownCards[card.key];
                    break;
            }
        };

        /**
         * Get shown cards values
         * @returns {Array}
         */
        Game.prototype.getShownCardsValues = function() {
            if (typeof Object.values !== 'function') {
                var values = [];

                for (var key in this.shownCards) {
                    values.push(this.shownCards[key]);
                }
                return values;
            }
            return Object.values(this.shownCards);
        };

        /**
         * Count of shown cards
         * @returns {Integer}
         */
        Game.prototype.getShownCardsCount = function() {
            return Object.keys(this.shownCards).length;
        };

        /**
         * Is end
         * @return {Boolean}
         */
        Game.prototype.isEnd = function() {
            var cardsCount = this._cards.length;
            for (var i = 0; i < cardsCount; i++) {
                if (this._cards[i].state !== 'removed') {
                    return false;
                }
            }
            return true;
        };

        /**
         * Export Game
         * @returns {Object} Can be stringify to JSON
         */
        Game.prototype.export = function() {
            return {
                cards: this._cards,
                state: this._state,
                shownCards: Object.keys(this.shownCards)
            };
        };

        return Game;
    }();

    // Class Memory
    var Memory = function() {
        function Memory(options) {
            this.options = $.extend(true, {
                controls: {
                    $newGameBtn:  $('#new-game'),
                    $gamePlace:   $('#game-place'),
                    $infoPlace:   $('#info-place'),
                    $winnerModal: $('#winner-modal')
                },
                cardsClasses:   ['fa-anchor', 'fa-archive', 'fa-asterisk', 'fa-at', 'fa-balance-scale', 'fa-bank', 'fa-bar-chart', 'fa-bed', 'fa-beer', 'fa-bell', 'fa-bicycle', 'fa-binoculars', 'fa-birthday-cake', 'fa-blind', 'fa-bolt', 'fa-bomb', 'fa-book', 'fa-briefcase', 'fa-bug', 'fa-bullhorn', 'fa-bus', 'fa-calculator', 'fa-camera', 'fa-car', 'fa-clock-o', 'fa-cloud', 'fa-coffee', 'fa-cogs', 'fa-comment', 'fa-compass', 'fa-credit-card', 'fa-cube', 'fa-cutlery', 'fa-dashboard', 'fa-database', 'fa-desktop', 'fa-diamond', 'fa-envelope', 'fa-exclamation-triangle', 'fa-eye', 'fa-eyedropper', 'fa-fax', 'fa-female', 'fa-fighter-jet', 'fa-fire-extinguisher', 'fa-flag', 'fa-flask', 'fa-frown-o', 'fa-futbol-o', 'fa-gavel', 'fa-gift', 'fa-glass', 'fa-globe', 'fa-graduation-cap', 'fa-group', 'fa-hashtag', 'fa-heart', 'fa-home', 'fa-image', 'fa-industry', 'fa-institution', 'fa-key', 'fa-keyboard-o', 'fa-laptop', 'fa-leaf', 'fa-life-bouy', 'fa-lightbulb-o', 'fa-lock', 'fa-magic', 'fa-magnet', 'fa-male', 'fa-money', 'fa-moon-o', 'fa-motorcycle', 'fa-music', 'fa-newspaper-o', 'fa-paint-brush', 'fa-paw', 'fa-pencil', 'fa-phone', 'fa-pie-chart', 'fa-plane', 'fa-plug', 'fa-puzzle-piece', 'fa-road', 'fa-rocket', 'fa-shield', 'fa-ship', 'fa-shopping-bag', 'fa-shopping-basket', 'fa-shopping-cart', 'fa-smile-o', 'fa-star', 'fa-suitcase', 'fa-thumb-tack', 'fa-thumbs-o-up', 'fa-tint', 'fa-trash', 'fa-tree', 'fa-trophy', 'fa-truck', 'fa-umbrella', 'fa-wheelchair', 'fa-wrench'],
                minPairs:       5,
                minNameLength:  1,
                maxNameLength:  10,
                minCompetitors: 2,
                storage:        null // cookie storage can "add storage && save" later
            }, options);

            this._competitors;
            this._game;
        }


        //  _____       _ _
        // |_   _|     (_) |
        //   | |  _ __  _| |_
        //   | | | '_ \| | __|
        //  _| |_| | | | | |_
        // |_____|_| |_|_|\__|
        //

        /**
         * Functionality initialization
         */
        Memory.prototype.init = function() {
            var lastSave = this.getLastSave(),
                self     = this;

            // Load last game (if is it possible)
            if (lastSave) {
                try {
                    this.loadGame(lastSave);
                }
                catch(e) {
                    if (confirm('Poslední uložená hra nelze nahrát, pravděpodobně byly poškozeny uložené záznamy. Přejete si poslední uloženou hru smazat?')) {
                        this.options.storage.remove('memory');
                    }
                }
            }

            // New game btn functionality
            this.options.controls.$newGameBtn
                    .click(function() {
                        self._game        = new Game('setup');
                        self._competitors = new Competitors();

                        // Delete old game from storage
                        if (self.options.storage)
                            self.options.storage.remove('memory');

                        // Clear info place
                        self.options.controls.$infoPlace.html('');

                        // Add player
                        self.renderAddPlayerForm(false);
                    })
                    .removeClass('disabled');

            // Add player btn
            this.options.controls.$gamePlace.on('click', '#add-player-btn', function(e) {
                self.clearErrors();

                var name = $('#player-name').val();

                // Validation
                if (name.length < self.options.minNameLength || name.length > self.options.maxNameLength) {
                    self.renderError('Jméno není povolená hodnota.');
                    return false;
                }

                // Unique
                if (self._competitors.findByName(name)) {
                    self.renderError('Jméno musí být unikátní.');
                    return false;
                }

                // Add player to game
                var player = self._competitors.addPlayer(name, 0, false);
                // Render player
                self.renderPlayer(player);

                // Next step
                self.renderAddPlayerForm(self._competitors.count() >= self.options.minCompetitors);

                e.preventDefault();
            });

            // Go to settings btn functionality
            this.options.controls.$gamePlace.on('click', '#settings-btn', function(e) {
                // New game settings
                self.renderSetDifficultyForm();

                e.preventDefault();
            });

            // Start game functionality
            this.options.controls.$gamePlace.on('click', '#start-game-btn', function(e) {
                self.clearErrors();

                var pairsCount = parseInt($('#pairs-count').val(), 10);

                // Validation
                if ( isNaN(pairsCount) || pairsCount < self.options.minPairs || pairsCount > self.options.cardsClasses.length) {
                    self.renderError('Počet párů není povolená hodnota.');
                    return false;
                }

                // Set & render cards
                var classes = self.options.cardsClasses.slice(0, pairsCount);
                var cardsContents = classes.concat(classes);

                for (var i = 0; i < cardsContents.length; i++) {
                    self._game.addCard(cardsContents[i], 'initial');
                }

                self._game.shuffleCards();

                self.renderCards(self._game.getCards());

                // Set & mark active player
                var player = self._competitors.nextInLine();

                self.renderActiveToPlayer(player);

                // Start game
                self._game.setState('canPlay');

                // Save game
                self.saveGame();

                e.preventDefault();
            });

            // Card functionality
            this.options.controls.$gamePlace.on('click', '#cards .card', function() {
                var $this       = $(this),
                    card        = self._game.getCard($this.data('key'));

                switch (self._game.getState()) {
                    case 'canPlay':
                        // Click on 'initial' card
                        if (self._game.getCardState(card) === 'initial') {
                            self._game.changeCardState(card, 'shown');
                            self.renderCardStateChange(card);
                            // If two cards shown
                            if (self._game.getShownCardsCount() === 2) {
                                self._game.setState('locked');
                                // Same cards
                                var shownCardsArray = self._game.getShownCardsValues();
                                if (shownCardsArray[0].content === shownCardsArray[1].content) {
                                    self._game.changeCardState(shownCardsArray[0], 'locked');
                                    self._game.changeCardState(shownCardsArray[1], 'locked');

                                    setTimeout(function() {
                                        for (var i = 0; i < shownCardsArray.length; i++) {
                                            self._game.changeCardState(shownCardsArray[i], 'removed');
                                            self.renderCardStateChange(shownCardsArray[i]);
                                        }

                                        if (self._game.isEnd()) {
                                            self._game.setState('done');
                                            self.renderWinners(self._competitors.getWinners());
                                        }
                                        else {
                                            self._game.setState('canPlay');
                                        }

                                        // Save game
                                        self.saveGame();
                                    }, 1000);

                                    self._competitors.active.score++;
                                    self.renderPlayerScoreChange(self._competitors.active);
                                }
                                else {
                                    self._game.setState('goBack');
                                }
                            }
                        }
                        break;

                    case 'locked':
                        break;

                    case 'goBack':
                        // Click on shown card
                        if (self._game.getCardState(card) === 'shown') {
                            self._game.changeCardState(card, 'initial');
                            self.renderCardStateChange(card);
                        }
                        // Next player
                        if (self._game.getShownCardsCount() === 0) {
                            var player = self._competitors.nextInLine();
                            self.renderActiveToPlayer(player);
                            self._game.setState('canPlay');
                        }
                        break;

                    case 'done':
                        break;
                }

                // Save game
                self.saveGame();
            });
        };


        //   _____                           _                 _
        //  / ____|                    _    | |               | |
        // | (___   __ ___   _____   _| |_  | | ___   __ _  __| |
        //  \___ \ / _` \ \ / / _ \ |_   _| | |/ _ \ / _` |/ _` |
        //  ____) | (_| |\ V /  __/   |_|   | | (_) | (_| | (_| |
        // |_____/ \__,_| \_/ \___|         |_|\___/ \__,_|\__,_|
        //
        
        /**
         * Get last save
         * @return {Object|null}
         */
        Memory.prototype.getLastSave = function() {
            try {
                return this.options.storage.getStruct('memory');
            }
            catch ($e) {
                return null;
            }
        };

        /**
         * Load game
         * @param {Object} savedGame
         * @throws {Error}
         */
        Memory.prototype.loadGame = function(savedGame) {
            this._game        = new Game('locked');
            this._competitors = new Competitors();
            this.options      = $.extend(true, this.options, {
                controls: {
                    $newGameBtn:  $(savedGame.options.controls.newGameBtnSelector),
                    $gamePlace:   $(savedGame.options.controls.gamePlaceSelector),
                    $infoPlace:   $(savedGame.options.controls.infoPlaceSelector),
                    $winnerModal: $(savedGame.options.controls.winnerModalSelector)
                },
                cardsClasses:   savedGame.options.cardsClasses,
                minPairs:       savedGame.options.minPairs,
                minNameLength:  savedGame.options.minNameLength,
                maxNameLength:  savedGame.options.maxNameLength,
                minCompetitors: savedGame.options.minCompetitors
            });

            // Clear info place
            this.options.controls.$infoPlace.html('');

            // Add competitors
            for (var i = 0; i < savedGame.competitors.players.length; i++) {
                var playerJSON = savedGame.competitors.players[i];
                // Add player to game
                var player = this._competitors.addPlayer(playerJSON.name, playerJSON.score, playerJSON.active);
                // Render player
                this.renderPlayer(player);
            }
            // Set & mark active player
            this._competitors.active = this._competitors.getPlayer(savedGame.competitors.active);
            this.renderActiveToPlayer(this._competitors.active);

            // Add cards
            var cardsCount = savedGame.game.cards.length;
            for (var i = 0; i < cardsCount; i++) {
                var cardJSON = savedGame.game.cards[i];
                // Add card to game
                this._game.addCard(cardJSON.content, cardJSON.state);
            }
            // Set & mark active cards
            for (var i = 0; i < savedGame.game.shownCards.length; i++) {
                var shownCardsIndex = savedGame.game.shownCards[i];
                this._game.shownCards[shownCardsIndex] = this._game.getCard(shownCardsIndex);
            }

            // Render cards
            this.renderCards(this._game.getCards());

            // Set game state
            this._game.setState(savedGame.game.state);

            // Done
            if (savedGame.game.state === 'done')
                this.renderWinners(this._competitors.getWinners());
        };

        /**
         * Save game if is it possible
         * @returns {Boolean}
         */
        Memory.prototype.saveGame = function() {
            var possibleStates = ['canPlay', 'goBack', 'done'];
            // Can't save
            if (this._game instanceof Game === false || possibleStates.indexOf(this._game.getState()) === -1 || !this.options.storage)
                return false;

            this.options.storage.setStruct('memory', {
                competitors: this._competitors.export(),
                game:        this._game.export(),
                options: {
                    controls: {
                        newGameBtnSelector:  this.options.controls.$newGameBtn.selector,
                        gamePlaceSelector:   this.options.controls.$gamePlace.selector,
                        infoPlaceSelector:   this.options.controls.$infoPlace.selector,
                        winnerModalSelector: this.options.controls.$winnerModal.selector
                    },
                    cardsClasses:   this.options.cardsClasses,
                    minPairs:       this.options.minPairs,
                    minNameLength:  this.options.minNameLength,
                    maxNameLength:  this.options.maxNameLength,
                    minCompetitors: this.options.minCompetitors,
                    storage:        Boolean(this.options.storage)
                }
            });

            return true;
        };


        //  ______
        // |  ____|
        // | |__   _ __ _ __ ___  _ __ ___
        // |  __| | '__| '__/ _ \| '__/ __|
        // | |____| |  | | | (_) | |  \__ \
        // |______|_|  |_|  \___/|_|  |___/
        //

        /**
         * Add error
         * @param {String} error
         */
        Memory.prototype.renderError = function(error) {
            this.options.controls.$gamePlace.prepend('<div class="alert alert-danger" role="alert"><i class="fa fa-frown-o"></i> <strong>Chyba!</strong> ' + error + '</div>');
        };

        /**
         * Clear errors
         */
        Memory.prototype.clearErrors = function() {
            this.options.controls.$gamePlace.find('.alert').remove();
        };


        //  _____                _              __
        // |  __ \              | |            / _|
        // | |__) |___ _ __   __| | ___ _ __  | |_ ___  _ __ _ __ ___  ___
        // |  _  // _ \ '_ \ / _` |/ _ \ '__| |  _/ _ \| '__| '_ ` _ \/ __|
        // | | \ \  __/ | | | (_| |  __/ |    | || (_) | |  | | | | | \__ \
        // |_|  \_\___|_| |_|\__,_|\___|_|    |_| \___/|_|  |_| |_| |_|___/
        //

        /**
         * Render form for adding player
         * @param {Boolean} canStart
         */
        Memory.prototype.renderAddPlayerForm = function(canStart) {
            this.options.controls.$gamePlace.html('\
                <h1>Přidání hráče</h1>\
                <form>\
                    <fieldset class="form-group">\
                        <label for="player-name">Jméno hráče</label>\
                        <input type="text" placeholder="Vyplňte jméno nebo přezdívku" name="player-name" id="player-name" class="form-control" maxlength="' + this.options.maxNameLength + '" autofocus>\
                    </fieldset>\
                    <button id="add-player-btn" class="btn btn-primary">Přidej dalšího hráče</button> ' +
                    (canStart ? '<button id="settings-btn" class="btn btn-success">Pokračuj</button>' : '') + '\
                </form>\
            ');
            this.options.controls.$gamePlace.find('input[autofocus]').focus();
        };

        /**
         * Render form for pairs count
         */
        Memory.prototype.renderSetDifficultyForm = function() {
            this.options.controls.$gamePlace.html('\
                <h1>Nestavení obtížnosti</h1>\
                <form>\
                    <fieldset class="form-group">\
                        <label for="pairs-count">Počet párů pexesa</label>\
                        <input type="number" placeholder="Polovina počtu všech hracích karet" name="pairs-count" id="pairs-count" class="form-control" min="' + this.options.minPairs + '" max="' + this.options.cardsClasses.length + '" autofocus>\
                        <small class="text-muted">Číslo musí být mezi ' + this.options.minPairs + ' a ' + this.options.cardsClasses.length + '<small>\
                    </fieldset>\
                    <button id="start-game-btn" class="btn btn-success">Začni hrát</button>\
                </form>\
            ');
            this.options.controls.$gamePlace.find('input[autofocus]').focus();
        };


        //  _____                _                                                           _
        // |  __ \              | |                                                         | |
        // | |__) |___ _ __   __| | ___ _ __    __ _  __ _ _ __ ___   ___   _ __   __ _ _ __| |_ ___
        // |  _  // _ \ '_ \ / _` |/ _ \ '__|  / _` |/ _` | '_ ` _ \ / _ \ | '_ \ / _` | '__| __/ __|
        // | | \ \  __/ | | | (_| |  __/ |    | (_| | (_| | | | | | |  __/ | |_) | (_| | |  | |_\__ \
        // |_|  \_\___|_| |_|\__,_|\___|_|     \__, |\__,_|_| |_| |_|\___| | .__/ \__,_|_|   \__|___/
        //                                      __/ |                      | |
        //                                     |___/                       |_|

        /**
         * Render player box
         * @param {Object} player
         */
        Memory.prototype.renderPlayer = function(player) {
            this.options.controls.$infoPlace.append('\
                <div id="player-' + player.id + '" class="player-info card">\
                    <div class="card-block">\
                        <h4 class="card-title">' + player.name + '</h4>\
                        <p class="card-text">Skóre: <span class="score pull-right label label-primary">' + player.score + '</span></p>\
                    </div>\
                </div>\
            ');
        };

        /**
         * Render and show winner modal
         * @param {Array} players
         */
        Memory.prototype.renderWinners = function(players) {
            var modal = this.options.controls.$winnerModal;

            modal.find('.modal-body').html('<p>Vyhrál hráč <strong class="label label-success">' + players[0].name + '</strong> s počtem bodů <span class="label label-success">' + players[0].score + '</span></p><p><i class="fa fa-trophy fa-5x"></i></p>');
            modal.modal('show');
        };

        /**
         * Highlight active player
         * @param {Object} player
         */
        Memory.prototype.renderActiveToPlayer = function(player) {
            this.options.controls.$infoPlace.find('.player-info').removeClass('active');

            $('#player-' + player.id).addClass('active');
        };

        /**
         * Render player score
         * @param {Object} player
         */
        Memory.prototype.renderPlayerScoreChange = function(player) {
            $('#player-' + player.id).find('.score').html(player.score);
        };

        /**
         * Render cards
         * @param {Array} cards
         */
        Memory.prototype.renderCards = function(cards) {
            var $cards = $('<div id="cards" class="row">');

            for (var i = 0; i < cards.length; i++) {
                $cards.append('\
                    <div class="col">\
                        <div id="card-' + cards[i].key + '" class="card' + (cards[i].state === 'removed' ? ' removed' : '') + '" data-key="' + i + '">\
                            <div class="card-block">' +
                                this.getCardContent(cards[i]) + '\
                            </div>\
                        </div>\
                    </div>\
                ');
            }

            this.options.controls.$gamePlace.html($cards);
        };

        /**
         * Get content of given card (front side or back side depending on card state)
         * @param {Object} card
         * @returns {String} Html
         */
        Memory.prototype.getCardContent = function(card) {
            return card.state === 'initial'
                   ? '<span>' + (card.key + 1) + '</span>'
                   : '<i class="fa ' + card.content + '"></i>';
        };

        /**
         * Render card content by state
         * @param {Object} card
         */
        Memory.prototype.renderCardStateChange = function(card) {
            var $card = $('#card-' + card.key);

            if (card.state === 'removed') {
                $card.addClass('removed');
            }

            $card.find('.card-block').html(this.getCardContent(card));
        };

        return Memory;
    }();

    window.Memory = Memory;
}(jQuery);