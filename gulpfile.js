// TODO: images z bower component (zatim jen kopirovat, pozdeji mozna zkombinovat s package "main-bower-files" nebo "wiredep")
var gulp        = require("gulp"),
    $           = require("gulp-load-plugins")(), //load gulp plugins ("$.useref" pouzije plugin "gulp-useref")
    del         = require("del"),
    browserSync = require("browser-sync");

//gulp.task('clean', del.bind(null, ['.tmp', 'dist']));
gulp.task("clean", function(cb) {
    del(["dist", ".tmp"], cb, {force: true});
});

gulp.task("fonts", ["clean"], function() {
    return gulp.src(["bower_components/**/*.{eot,svg,ttf,woff,woff2}", "src/fonts/**/*.{eot,svg,ttf,woff,woff2}"])
            .pipe($.rename({ dirname: ""})) // remove structure from bower_components
            .pipe(gulp.dest(".tmp/fonts"))
            .pipe(gulp.dest("dist/fonts"));
});

gulp.task("images", ["clean"], function() {
    return gulp.src(["src/images/**/*.*"])
            .pipe(gulp.dest(".tmp/images"))
            .pipe(gulp.dest("dist/images"));
});

gulp.task("scss", ["clean"], function() {
    return gulp.src(["src/scss/**/*.scss"])
            .pipe(gulp.dest(".tmp/scss"));
});

gulp.task("production", ["fonts", "images"], function() {
    var assets = $.useref.assets();
    
    return gulp.src("src/*.html")
            .pipe(assets) //-----------------------------------------gulp-useref
            
            .pipe($.if('*.js', $.uglify()))
            .pipe($.if('*.css', $.sass()))
            .pipe($.if('*.css', $.minifyCss()))
            .pipe($.rev()) // add revision hash to files
            
            .pipe(assets.restore()) //-------------------------------gulp-useref
            .pipe($.useref()) //-------------------------------------gulp-useref
            
            .pipe($.revReplace()) // add revision hash to HTML files (header and footer tags)
            .pipe(gulp.dest("dist"));
});

gulp.task("devel", ["fonts", "images", "scss"], function() {
    // TODO: rozhodnout se ktera cesta je lepsi, jestli takovato, nebo zprovozneni HTML souboru primo v adresari SRC pomoci nahtani skriptu pro transformaci SCSS -> CSS
    var assets = $.useref.assets({ noconcat: true });
    
    return gulp.src("src/*.html")
            .pipe(assets) //-----------------------------------------gulp-useref
            
            //.pipe($.if('*.scss', $.rename({ dirname: "css"})))
            .pipe($.if('*.scss', $.sass( { errLogToConsole: true } ))) // { includePaths: ['./src/sass'], errLogToConsole: true } 
            .pipe($.if(function (file) {
                return (file.path.substr(-4) === ".css" && file.path.indexOf("bower_components") === -1);
            }, $.rename({ dirname: "css"})))
            .pipe($.if(function (file) {
                return (file.path.indexOf("bower_components") === -1);
            }, $.rev())) // add revision hash to files
            
            .pipe(assets.restore()) //-------------------------------gulp-useref
            //.pipe($.useref()) //-------------------------------------gulp-useref
             
            .pipe($.if("*.html", $.replace(/<?link.*?href=["']scss\/(.*?)\.scss["'].*?(?:>|\))/gi, "<link href=\"css/$1.css\" rel=\"stylesheet\">"))) // replace scss link tags 
            .pipe($.revReplace()) // add revision hash to HTML files (header and footer tags)
            .pipe(gulp.dest(".tmp"));
});

gulp.task("reloadDevel", ["devel"], browserSync.reload);

gulp.task("serve", ["devel"], function() {
    browserSync({
        notify: false,
        port: 9000,
        server: {
            baseDir: ['.tmp', 'src'],
            routes: {
                '/bower_components': 'bower_components'
            }
        }
    });
    
    gulp.watch([
        'src/*.html',
        'src/js/**/*.js',
        'src/scss/**/*.scss',
        'src/css/**/*.css',
	
	'src/fonts/**/*',
	'src/images/**/*'
    ], ["reloadDevel"]);//.on('change', ["reloadDevel"]);
    
//!    gulp.watch('src/fonts/**/*', ['fonts']);
//!    gulp.watch('src/images/**/*', ['images']);
    //gulp.watch('bower.json', ['wiredep', 'fonts']);
});

gulp.task("default", ["serve"]);
